from time import sleep

print('')
a = int(input('Primeiro valor: '))
b = int(input('Segundo valor: '))
choice = 0

while choice != 7:
    sleep(0.5)
    print('')
    print('\033[1;32m-=-\033[m' * 30)
    print('')
    print('''        [1] Somar
        [2] Subtrair
        [3] Multiplicar
        [4] Elevar
        [5] Maior
        [6] Novos Números
        [7] Sair do Programa
         ''')
    choice = int(input('>>>> Qual é a sua opção?: '))
    print('')
    if choice == 1:
        print('A soma entre {} e {} é igual a {}'.format(a, b, a+b))
    elif choice == 2:
        print('A subtração entre {} e {} é igual a {}'.format(a, b, a-b))
    elif choice == 3:
        print('A multiplicação entre {} e {} é igual a {}'.format(a, b, a*b))
    elif choice == 4:
        print('A potenciação de {} por {} é igual a {}'.format(a, b, a**b))
    elif choice == 5:
        if a > b:
            print('Entre {} e {}, o maior é {}'.format(a, b, a))
        elif a == b:
            print('Os números {} e {} são iguais'.format(a, b))
        else:
            print('Entre {} e {}, o maior é {}'.format(a, b, b))
    elif choice == 6:
        del a
        del b
        a = int(input('Primeiro valor: '))
        b = int(input('Segundo valor: '))
    elif choice == 7:
        print('Finalizando...')
    else:
        print('Número inválido. Tente novamente')
sleep(1)
print('Fim do programa! Volte Sempre!')
